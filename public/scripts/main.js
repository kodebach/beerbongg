/*
*
*  Push Notifications codelab
*  Copyright 2015 Google Inc. All rights reserved.
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      https://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License
*
*/

/* eslint-env browser, es6 */

'use strict';

const messaging = firebase.messaging();
messaging.usePublicVapidKey("BGmx7d04dJvxM1JB7gUfWbErvEjgK-v8G2K9f9XMWkU_E8_FE9M0G37RrHA1CZfJ31NRT6de0UV5dyeHRyp7qNI");

function enableForm() {
  const form = document.querySelector("#main-form");
  const btn = document.querySelector("#submit-btn")

  const chkHl = document.querySelector("#hausleiten");
  const chkSt = document.querySelector("#stetteldorf");

  btn.value = "Erinnerungen aktivieren";
  chkHl.disabled = false;
  chkSt.disabled = false;
  form.onsubmit = enableNotifications;
}

function disableForm() {
  const form = document.querySelector("#main-form");
  const btn = document.querySelector("#submit-btn")

  const chkHl = document.querySelector("#hausleiten");
  const chkSt = document.querySelector("#stetteldorf");

  btn.value = "Erinnerungen deaktivieren";
  chkHl.disabled = true;
  chkSt.disabled = true;
  form.onsubmit = disableNotifications;
}

function registerWithServer(token) {
  const chkHl = document.querySelector("#hausleiten");
  const chkSt = document.querySelector("#stetteldorf");

  const register = firebase.functions().httpsCallable('register');
  return register({ token: token, hausleiten: chkHl.checked, stetteldorf: chkSt.checked }).then(function (result) {
    return true;
  }).catch(function (error) {
    return false;
  });
}

function unregisterFromServer(token) {
  const register = firebase.functions().httpsCallable('unregister');
  return register({ token: token }).then(function (result) {
    return true;
  }).catch(function (error) {
    return false;
  });
}

function enableNotifications(event) {
  event.preventDefault();

  const chkHl = document.querySelector("#hausleiten");
  const chkSt = document.querySelector("#stetteldorf");

  if (!chkHl.checked && !chkSt.checked) {
    return;
  }


  function register() {
    messaging.getToken().then(function (currentToken) {
      if (currentToken) {
        return registerWithServer(currentToken);
      } else {
        return false;
      }
    }).then(function (success) {
      if (success) {
        window.localStorage.setItem("registered", "1");
        window.localStorage.setItem("hausleiten", chkHl.checked);
        window.localStorage.setItem("stetteldorf", chkSt.checked);
        disableForm();
      } else {
        enableForm();
      }
    }).catch(function (err) {
      console.error('Unable to get permission to notify.', err);
    });
  }

  if (Notification.permission === "granted") {
    register();
  } else {
    Notification.requestPermission().then(function (permission) {
      if (permission === "granted") {
        register();
      }
    });
  }
}

function disableNotifications(event) {
  event.preventDefault();

  messaging.getToken().then(function (currentToken) {
    if (currentToken) {
      return unregisterFromServer(currentToken);
    } else {
      return false;
    }
  }).then(function (success) {
    if (success) {
      messaging.deleteToken();
      window.localStorage.setItem("registered", "0");
      window.localStorage.setItem("hausleiten", false);
      window.localStorage.setItem("stetteldorf", false);
      enableForm();
    } else {
      disableForm();
    }
  }).catch(function (err) {
    console.error('Unable to get token to unregister.', err);
  });
}

if (window.localStorage.getItem("registered") === "1") {
  disableForm();
} else {
  enableForm();
}

function init() {
  const chkHl = document.querySelector("#hausleiten");
  const chkSt = document.querySelector("#stetteldorf");

  chkHl.checked = window.localStorage.getItem("hausleiten");
  chkSt.checked = window.localStorage.getItem("stetteldorf");

  messaging.onTokenRefresh(function () {
    if (window.localStorage.getItem("registered") === "1") {
      messaging.getToken().then(function (currentToken) {
        if (currentToken) {
          return updateServerToken(currentToken);
        }
      });
    } else {
      messaging.deleteToken();
    }
  });

  messaging.onMessage(function (payload) {
    new Notification(payload.notification.title, {body: payload.notification.body});
  });
}

init();