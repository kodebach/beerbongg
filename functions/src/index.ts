import * as functions from 'firebase-functions';
import admin = require('firebase-admin');

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

admin.initializeApp();

export const sendNotification = functions.pubsub.schedule('0 0-2,8-23 * * *').timeZone('Europe/Vienna').onRun(async (context) => {
    await admin.messaging().send({
        topic: "hausleiten", 
        notification: {
            title: "Stündliche Erinnerung (Hausleiten)",
            body: "Bitte abstimmen!"
        },
        webpush: {
            fcmOptions: {
                link: "https://www.ligaportal.at/noe/1-klasse/1-klasse-nordwest/82298-1-klasse-nordwest-waehle-das-beliebteste-hausbaufuehrer-at-unterhaus-team-2020"
            }
        }
    });

    await admin.messaging().send({
        topic: "stetteldorf", 
        notification: {
            title: "Stündliche Erinnerung (Stetteldorf)",
            body: "Bitte abstimmen!"
        },
        webpush: {
            fcmOptions: {
                link: "https://www.ligaportal.at/noe/2-klasse/2-klasse-donau/82306-2-klasse-donau-waehle-das-beliebteste-hausbaufuehrer-at-unterhaus-team-2020"
            }
        }
    });
});

export const register = functions.https.onCall(async (data, context) => {
    if(data.hausleiten) {
        await admin.messaging().subscribeToTopic(data.token, "hausleiten");
    }

    if(data.stetteldorf) {
        await admin.messaging().subscribeToTopic(data.token, "stetteldorf");
    }
});

export const unregister = functions.https.onCall(async (data, context) => {
    if(data.hausleiten) {
        await admin.messaging().unsubscribeFromTopic(data.token, "hausleiten");
    }

    if(data.stetteldorf) {
        await admin.messaging().unsubscribeFromTopic(data.token, "stetteldorf");
    }
});